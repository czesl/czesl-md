# czesl-md

The repository includes a subset of CzeSL-man, a learner corpus of non-native Czech, semi-automatically annotated to specify: 

1. Corrections (target hypotheses)

2. Morphemic boundaries between stems and inflectional affixes

3. Error types, based on several linguistic domains with a focus on morphemics and morphology, see the Annotation manual (<http://utkl.ff.cuni.cz/~rosen/public/2018_prirucka_morfologicke_anotace.pdf>,  in Czech)

The texts are stored in a format produced by brat (<http://brat.nlplab.org>), an annotation editor. 

Eventually, this annotation will be extended to the whole CzeSL corpus and merged with all other types of annotation available for the CzeSL corpus: 

- General annotation of CzeSL-man
- Automatic annotation of CzeSL-SGT
- Syntactic annotation of CzeSL-UD

For more details see <http://utkl.ff.cuni.cz/learncorp/>.

Currently, the repository includes annotation produced in Phase 1: 10 thousand words in 18 texts, annotated independently by two annotators.
